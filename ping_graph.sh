#!/usr/bin/env bash

RRDTOOL=rrdtool
RRDTOOL_OPTS="-a PNG --rigid --lower-limit 0 --imginfo '<IMG SRC=%s WIDTH=%lu HEIGHT=%lu >' -v 'Round-Trip Time (ms)'"

RRD_DIR=.
OUTPUT_DIR=
VERBOSE=0

function usage() {
  echo "Usage: `basename $0` [-d rrd_dir] [-o output_dir] [-v level]"
  echo ""
  echo -e "-d rrd_dir\t\tDirectory where RRD files are stored"
  echo -e "-o output_dir\t\tDirectory in which to write HTML and graphs"
  echo -e "-v level\t\tEnable verbose logging at level <level>"
  exit $1
}

function verbose_log() {
  if [ $VERBOSE -ge $1 ]; then
    echo $2;
  fi
}

function run_rrd() {
  if [ -n "$5" ]; then
    XGRID="--x-grid $5"
  fi

  $RRDTOOL graph "$2" -h 225 -w 600 -a PNG \
  --imginfo '<IMG SRC=%s WIDTH=%lu HEIGHT=%lu >' \
  --lazy --start $3 --end $4 $XGRID \
  -v "Round-Trip Time (ms)" \
  --rigid \
  --lower-limit 0 \
  "DEF:roundtrip=$1:rtt:AVERAGE" \
  "DEF:packetloss=$1:pl:AVERAGE" \
  VDEF:last=roundtrip,LAST \
  CDEF:PLNone=packetloss,0,2,LIMIT,UN,UNKN,INF,IF \
  CDEF:PL2=packetloss,2,8,LIMIT,UN,UNKN,INF,IF \
  CDEF:PL15=packetloss,8,15,LIMIT,UN,UNKN,INF,IF \
  CDEF:PL25=packetloss,15,25,LIMIT,UN,UNKN,INF,IF \
  CDEF:PL50=packetloss,25,50,LIMIT,UN,UNKN,INF,IF \
  CDEF:PL75=packetloss,50,75,LIMIT,UN,UNKN,INF,IF \
  CDEF:PL100=packetloss,75,100,LIMIT,UN,UNKN,INF,IF \
  AREA:roundtrip#4444ff:"Round Trip Time (millis)" \
  GPRINT:roundtrip:LAST:"Cur\: %5.2lf" \
  GPRINT:roundtrip:AVERAGE:"Avg\: %5.2lf" \
  GPRINT:roundtrip:MAX:"Max\: %5.2lf" \
  GPRINT:roundtrip:MIN:"Min\: %5.2lf\n" \
  AREA:PLNone#6c9bcd:"0-2%":STACK \
  AREA:PL2#00ffae:"2-8%":STACK \
  AREA:PL15#ccff00:"8-15%":STACK \
  AREA:PL25#ffff00:"15-25%":STACK \
  AREA:PL50#ffcc66:"25-50%":STACK \
  AREA:PL75#ff9900:"50-75%":STACK \
  AREA:PL100#ff0000:"75-100%":STACK \
  COMMENT:"(Packet Loss Percentage)\n" \
  COMMENT:\\s \
  GPRINT:last:"Last sample time\: %+\c:strftime"
}

while getopts "d:o:v:" Option
do
  case $Option in
    d)
      RRD_DIR="$OPTARG"
      ;;
    o)
      OUTPUT_DIR="$OPTARG"
      ;;
    v)
      VERBOSE=$OPTARG
      ;;
    ?)
      echo "Unknown option: $Option"
      usage -85
      ;;
  esac
done
shift $(($OPTIND - 1))

if [ -z "$OUTPUT_DIR" ]; then
  verbose_log 1 "Defaulting output directory to RRD directory"
  OUTPUT_DIR="${RRD_DIR}"
fi

echo "
<HTML>
<HEAD><TITLE>Round-Trip and Packet Loss Stats</TITLE></HEAD>
<BODY>
<H2>Round-Trip & Packetloss Stats</H2>
<UL>
" > "${OUTPUT_DIR}/index.html"


for RRD_FILE in `cd "${RRD_DIR}" && ls *.rrd`; do
  HOST="${RRD_FILE%.rrd}"
  OUTPUT_FILE_PREFIX="ping_${HOST}"

  verbose_log 1 "Processing ${HOST} (${RRD_DIR}/${RRD_FILE}) into ${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  echo "
  <LI><A HREF=\"${OUTPUT_FILE_PREFIX}.html\">${HOST}</A></LI>
  " >> "${OUTPUT_DIR}/index.html"

  echo "
  <HTML>
  <HEAD><TITLE>Round-Trip and Packet Loss Stats (${HOST})</TITLE></HEAD>
  <META HTTP-EQUIV="refresh" CONTENT="300"></META>
  <BODY>
  <H2>Round-Trip & Packetloss Stats (${HOST})</H2>
  <H3>Last hour</H3>
  " > "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  run_rrd "${RRD_DIR}/${RRD_FILE}" "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}_hour.png" -3600 -60 >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  echo "
  <br><br>
  <H3>Last day</H3>
  " >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  run_rrd "${RRD_DIR}/${RRD_FILE}" "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}_day.png" -86400 -60 >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  echo "
  <br><br>
  <H3>Last week</H3>
  " >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  run_rrd "${RRD_DIR}/${RRD_FILE}" "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}_week.png" -604800 -1800 >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  echo "
  <br><br>
  <H3>Last month</H3>
  " >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  run_rrd "${RRD_DIR}/${RRD_FILE}" "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}_month.png" -2592000 -7200 >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  echo "
  <br><br>
  <H3>Last year</H3>
  " >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  run_rrd "${RRD_DIR}/${RRD_FILE}" "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}_year.png" -31536000 -86400 >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"

  echo "
  <br><br>
  </BODY>
  </HTML>
  " >> "${OUTPUT_DIR}/${OUTPUT_FILE_PREFIX}.html"
done

echo "
</UL>
</BODY>
</HTML>
" >> "${OUTPUT_DIR}/index.html"

